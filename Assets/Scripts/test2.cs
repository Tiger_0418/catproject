﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test2 : MonoBehaviour {
    public void play_wave()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"wave_once\"}");
    }
    public void play_bow()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!\",\"model\":\"bow_once\"}");
    }
    public void play_right()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"right_start\"}");
    }
    public void play_left()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"left_start\"}");
    }
    public void play_repeat()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"repeat_start\"}");
    }
    public void play_talk1()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"talk_1_start\"}");
    }
    public void play_talk2()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"talk_2_start\"}");
    }
    public void play_talk3()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"talk_3_start\"}");
    }
    public void play_idle()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":\"QB_B000_idle\"}");
    }

    public void showphoto()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.ShowPhotoText("{\"model\":[\"talk_1_start\"],\"imgurl\":\"https://image.gameflier.com/PB/2018092003.jpg\",\"talks\":\"hihi!!!吃飯要吃飽飽的喔\"}");
    }

    public void playvideo()
    {
        MyQbi2 myqb = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        myqb.ShowVideo("{\"model\":[\"talk_1_start\"],\"videoname\":\"\",\"imgurl\":\"https://image.gameflier.com/PB/2018092003.jpg\",\"youtubeurl\":\"https://qbi.chainsea.com.tw/gateway/custom/videoplayback.mp4\"}");
    }
}
