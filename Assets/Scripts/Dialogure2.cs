﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Dialogure2 : MonoBehaviour {
    public Textfad2 textfad = null;
    public float textspeed = 0.2f;
    public float pagespeed = 0.5f;
    private RawImage imageToDisplay;

    private void Start()
    {
        //say("請問您要詢問的是<link=\"action= 高鐵網路訂票 \">高鐵</link>還是<link=\"action= 台鐵網路訂票 \">台鐵</link>的網路訂票？");        
        //say("請洽詢經濟部青創貸款資訊專區\n\n經濟部中小企業部\n服務專線：0800-056-476 (馬上辦服務中心)\n機關地址：10646 臺北市羅斯福路二段95號3樓\n網址：<link=\"url=https://bit.ly/2RhkQKP\">https://www.moeasmea.gov.tw/ct.asp?xItem=11715&ctNode=609&mp=1</link>","1", "-300", "-47", "80","-47");
    }

    public void moveObject(string x, string y, string size)
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "photo_border")
            {
                float xpos = float.Parse(x);
                float ypos = float.Parse(y);
                float scale = float.Parse(size);
                s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(xpos, ypos, 0);
                s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                break;
            }
        }
    }

    public void showPhotoText(string imgurl, string talks, string x, string y, string size)
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "photo_border")
            {
                float xpos = float.Parse(x);
                float ypos = float.Parse(y);
                float scale = float.Parse(size);
                s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(xpos, ypos, 0);
                s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                break;
            }
        }
        StartCoroutine(LoadImage(imgurl, talks));
    }

    private IEnumerator LoadImage(string loadedURL, string talks)
    {
        WWW www = new WWW(loadedURL);
        while (!www.isDone)
        {
            Debug.Log("Download image on progress" + www.progress);
            yield return null;
        }

        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "photo_border")
            {
                s.gameObject.SetActive(true);
                imageToDisplay = s.Find("photo_img").GetComponentInChildren<RawImage>();
                break;
            }
        }

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Download failed");
        }
        else
        {
            Debug.Log("Download succes");
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(www.bytes);
            texture.Apply();
            float show_w = texture.width, show_h = texture.height;
            Debug.Log("org_w:" + show_w+"|org_h:"+show_h);
            if(show_w > show_h) 
            {
                show_w = 905;
                if(texture.width > 905)
                {
                    show_h = (float)(((float)905/(float)texture.width) * (float)texture.height);                    
                }
                else
                {
                    show_h = (float)(((float)texture.width/(float)905) * (float)texture.height);
                }
            }
            else 
            {
                if(texture.height > 765)
                {
                    show_w = (float)(((float)765/(float)texture.height) * (float)texture.width);
                }
                else
                {
                    show_w = (float)(((float)texture.height/(float)765) * (float)texture.width);
                }
                show_h = 765;
            }

            Debug.Log("new_w:" + show_w+"|new_h:"+show_h);
            imageToDisplay.GetComponent<RectTransform>().sizeDelta = new Vector2(show_w, show_h);
            imageToDisplay.texture = texture;
            //imageToDisplay.GetComponent<RectTransform>().localPosition = new Vector3((-3 + (905/2)-(show_w/2)), 
            //(10.6f - (765/2)+(show_h/2)), 0);
        }

        phototext(talks);
    }

    private void phototext(string talks)
    {
        if (talks == null || talks.Length <= 0)
            return;

        if (textfad != null)
        {
            textfad.clear();
            textfad = null;
            StartCoroutine(phototext2(talks));
            return;
        }

        transform.Find("photo_border").gameObject.SetActive(true);
        textfad = transform.Find("photo_border").transform.Find("photodialog").gameObject.GetComponent<Textfad2>();
        textfad.showphototext(talks, textspeed, pagespeed);
    }

    IEnumerator phototext2(string talks)
    {
        yield return new WaitForSeconds(0.5f);

        transform.Find("photo_border").gameObject.SetActive(true);
        textfad = transform.Find("photo_border").transform.Find("photodialog").gameObject.GetComponent<Textfad2>();
        textfad.showphototext(talks, textspeed, pagespeed);
    }

    public void movesayObject(string x, string y, string size)
    {
        float scale = float.Parse(size);
        float fromx = float.Parse(x);
        float fromy = float.Parse(y);
        if (transform.Find("Dialog1-5").gameObject.activeSelf)
        {
            transform.Find("Dialog1-5").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog1-5").gameObject.GetComponent<RectTransform>().localScale
                = new Vector3(scale, scale, scale);
        }
        else if (transform.Find("Dialog5-40").gameObject.activeSelf)
        {
            transform.Find("Dialog5-40").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog5-40").gameObject.GetComponent<RectTransform>().localScale
                = new Vector3(scale, scale, scale);
        }
        else if (transform.Find("Dialog40-66").gameObject.activeSelf)
        {
            transform.Find("Dialog40-66").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog40-66").gameObject.GetComponent<RectTransform>().localScale
                = new Vector3(scale, scale, scale);
        }
        else if (transform.Find("Dialog66-108").gameObject.activeSelf)
        {
            transform.Find("Dialog66-108").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog66-108").gameObject.GetComponent<RectTransform>().localScale
                = new Vector3(scale, scale, scale);
        }
    }

    public void say(string talks, string size, string showpos_x, string showpos_y, string x, string y)
    {
        if (textfad != null)
        {
            textfad.clear();
            textfad = null;
            //StartCoroutine(says2(talks));
            //return;
        }

        float scale = float.Parse(size);
        float posx = float.Parse(showpos_x);
        float posy = float.Parse(showpos_y);
        float fromx = float.Parse(x);
        float fromy = float.Parse(y);

        string m_temp_content = talks;
        while (m_temp_content.IndexOf("<link") >= 0)
        {
            int start = m_temp_content.IndexOf("<link");
            int end = m_temp_content.IndexOf(">", start + 1);
            int start2 = m_temp_content.IndexOf("</link>", end + 1);

            m_temp_content = m_temp_content.Remove(start2, 7);
            m_temp_content = m_temp_content.Remove(start, end - start + 1);
        }
        int totallens = m_temp_content.ToCharArray().Length;

        if (totallens <= 5)
        {
            Textfad2 fad = transform.Find("Dialog1-5").gameObject.GetComponent<Textfad2>();
            fad.showpos = new Vector3(posx, posy, 0);
            fad.showscale = new Vector3(scale, scale, scale);
            transform.Find("Dialog1-5").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog1-5").gameObject.GetComponent<RectTransform>().localScale
= new Vector3(0.01f, 0.01f, 0.01f);
            transform.Find("Dialog1-5").gameObject.SetActive(true);
            textfad = transform.Find("Dialog1-5").gameObject.GetComponent<Textfad2>();
        }
        else if(totallens <= 40)
        {
            Textfad2 fad = transform.Find("Dialog5-40").gameObject.GetComponent<Textfad2>();
            fad.showpos = new Vector3(posx, posy, 0);
            fad.showscale = new Vector3(scale, scale, scale);
            transform.Find("Dialog5-40").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog5-40").gameObject.GetComponent<RectTransform>().localScale
= new Vector3(0.01f, 0.01f, 0.01f);
            transform.Find("Dialog5-40").gameObject.SetActive(true);
            textfad = transform.Find("Dialog5-40").gameObject.GetComponent<Textfad2>();
        }
        else if (totallens <= 66)
        {
            Textfad2 fad = transform.Find("Dialog40-66").gameObject.GetComponent<Textfad2>();
            fad.showpos = new Vector3(posx, posy, 0);
            fad.showscale = new Vector3(scale, scale, scale);
            transform.Find("Dialog40-66").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog40-66").gameObject.GetComponent<RectTransform>().localScale
= new Vector3(0.01f, 0.01f, 0.01f);
            transform.Find("Dialog40-66").gameObject.SetActive(true);
            textfad = transform.Find("Dialog40-66").gameObject.GetComponent<Textfad2>();
        }
        else if (totallens >= 66)
        {
            Textfad2 fad = transform.Find("Dialog66-108").gameObject.GetComponent<Textfad2>();
            fad.showpos = new Vector3(posx, posy, 0);
            fad.showscale = new Vector3(scale, scale, scale);
            transform.Find("Dialog66-108").gameObject.GetComponent<RectTransform>().localPosition
                = new Vector3(fromx, fromy, 0);
            transform.Find("Dialog66-108").gameObject.GetComponent<RectTransform>().localScale
= new Vector3(0.01f, 0.01f, 0.01f);
            transform.Find("Dialog66-108").gameObject.SetActive(true);
            textfad = transform.Find("Dialog66-108").gameObject.GetComponent<Textfad2>();
        }
        textfad.showDialog(talks, textspeed, pagespeed);
    }

    IEnumerator says2(string talks)
    {
        yield return new WaitForSeconds(0.5f);

        string m_temp_content = talks;
        while (m_temp_content.IndexOf("<link") >= 0)
        {
            int start = m_temp_content.IndexOf("<link");
            int end = m_temp_content.IndexOf(">", start + 1);
            int start2 = m_temp_content.IndexOf("</link>", end + 1);

            m_temp_content = m_temp_content.Remove(start2, 7);
            m_temp_content = m_temp_content.Remove(start, end - start + 1);
        }
        int totallens = m_temp_content.ToCharArray().Length;

        if (totallens <= 5)
        {
            transform.Find("Dialog1-5").gameObject.SetActive(true);
            textfad = transform.Find("Dialog1-5").gameObject.GetComponent<Textfad2>();
        }
        else if (totallens <= 40)
        {
            transform.Find("Dialog5-40").gameObject.SetActive(true);
            textfad = transform.Find("Dialog5-40").gameObject.GetComponent<Textfad2>();
        }
        else if (totallens <= 66)
        {
            transform.Find("Dialog40-66").gameObject.SetActive(true);
            textfad = transform.Find("Dialog40-66").gameObject.GetComponent<Textfad2>();
        }
        else if (totallens >= 66)
        {
            transform.Find("Dialog66-108").gameObject.SetActive(true);
            textfad = transform.Find("Dialog66-108").gameObject.GetComponent<Textfad2>();
        }
        textfad.showDialog(talks, textspeed, pagespeed);
    }

    public void closeDialog()
    {
        transform.Find("Dialog1-5").gameObject.SetActive(false);
        transform.Find("Dialog5-40").gameObject.SetActive(false);
        transform.Find("Dialog40-66").gameObject.SetActive(false);
        transform.Find("Dialog66-108").gameObject.SetActive(false);
    }
}
