﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Progress : MonoBehaviour
{

    public Image foregroundImage;

    public int Value
    {
        get
        {
            if (foregroundImage != null)
                return (int)(foregroundImage.fillAmount * 100);
            else
                return 0;
        }
        set
        {
            if (foregroundImage != null)
                foregroundImage.fillAmount = value / 100f;
        }
    }

    void Start()
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>();
        foreach(Transform s in trs)
        {
            if(s.name == "Foreground")
            {
                foregroundImage = s.GetComponent<Image>();
                break;
            }
        }
        Value = 0;
    }
}

