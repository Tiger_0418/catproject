﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonExit : MonoBehaviour
{
    private bool bclose = false;

    public void closeUnity()
    {
        //if (GameObject.Find("DialogCtl"))
        //{
        //    GameObject.Find("DialogCtl").SetActive(false);
        //    MyQbi qbi = GameObject.Find("Qbi").GetComponent<MyQbi>();
        //    if (qbi)
        //        qbi.stopAni();
        //    MyQbi2 qbi2 = GameObject.Find("Qbi").GetComponent<MyQbi2>();
        //    if (qbi2)
        //        qbi2.stopAni();
        //}
        GameObject.Find("QB").GetComponent<player>().AniPlay("B3001");
        //StartCoroutine(startclose());
        bclose = true;
        sendbtn5();
    }

    public void sendbtn1()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackBtn", new object[] { "1" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendbtn1");
        }
    }

    public void sendbtn2()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackBtn", new object[] { "2" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendbtn2");
        }
    }

    public void sendbtn3()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackBtn", new object[] { "3" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendbtn3");
        }
    }

    public void sendbtn4()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackBtn", new object[] { "4" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendbtn4");
        }
    }

    public void sendbtn5()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackBtn", new object[] { "5" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendbtn5");
        }
    }

    public void hideShowIdleBtn(string btnName)
    {
        Transform[] tr = null;
        tr = this.transform.Find("Canvas").GetComponentsInChildren<Transform>(true);
        if (tr != null)
        {
            foreach (Transform s in tr)
            {
                if (s.name == btnName)
                {
                    if (s.gameObject.activeSelf)
                        s.gameObject.SetActive(false);
                    else
                        s.gameObject.SetActive(true);
                    break;
                }
            }
        }
    }

    public void closeUnityAllWindows()
    {
        Transform tr = null, tr2 = null;
        tr = this.transform.Find("Canvas").Find("AudioCtl");
        if (tr != null)
        {
            tr2 = tr.Find("border");
            if (tr2)
            {
                tr2.gameObject.SetActive(false);
            }
        }
        tr = this.transform.Find("Canvas").Find("VideoCtl");
        if (tr != null)
        {
            tr2 = tr.Find("video_border");
            if (tr2)
            {
                tr2.gameObject.SetActive(false);
            }
        }
        tr = this.transform.Find("Canvas").Find("PhotoCtl");
        if (tr != null)
        {
            tr2 = tr.Find("photo_border");
            if (tr2)
            {
                tr2.gameObject.SetActive(false);
            }
        }

        tr = this.transform.Find("Canvas").Find("DialogCtl");
        if (tr != null)
        {
            Dialogure d1 = tr.gameObject.GetComponent<Dialogure>();
            if (d1)
            {
                d1.closeDialog();
                return;
            }
            Dialogure2 d2 = tr.gameObject.GetComponent<Dialogure2>();
            if (d2)
            {
                d2.closeDialog();
                return;
            }
        }
    }

    public void sendPlayPause()
    {
        AudioCtl ctl = this.transform.Find("Canvas").Find("AudioCtl").gameObject.GetComponent<AudioCtl>();
        if (ctl != null)
        {
            int mode = ctl.changeIcon();
            try
            {
                using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                {
                    unityClass.CallStatic("callPlayPauseAudio", new object[] { String.Format("{0}", mode) });
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message + "sendPlayPause");
            }
        }
    }

    IEnumerator startclose()
    {
        yield return new WaitForSeconds(2.0f);
        bclose = true;
    }

    private void sendClose()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                AndroidJavaObject currentActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
                currentActivity.Call("closeUnity");
            }          
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "sendClose");
        }
    }

    //callback到客戶端
    public void callbackapp(string message)
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                AndroidJavaObject currentActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
                currentActivity.Call("callBackApp", message);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "callbackapp");
        }
    }

    void Update()
    {
        if (bclose)
        {
            GameObject Qb = GameObject.Find("QB");
            Animator ani = Qb.GetComponent<Animator>();
            if (ani.GetCurrentAnimatorStateInfo(0).IsName("leave_1"))
            {
                if (ani.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.75f)
                {
                    bclose = false;
                    Qb.SetActive(false);
                    DestroyImmediate(Qb, true);
                    Debug.Log("closing");
                    //Destroy(GameObject.Find("Main"));
                    sendClose();
                    //Application.Quit();
                }
            }
        }
    }
}
