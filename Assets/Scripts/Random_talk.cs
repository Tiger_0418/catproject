﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Random_talk : MonoBehaviour {

	Animator ani;
	int ran_num;
	int check;

	// Use this for initialization
	void Start () 
	{
		ani = GetComponent<Animator>();        
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (ani.GetCurrentAnimatorStateInfo (0).IsName ("Action.Random_talk") && check == 0) 
		{
			ran_num = Random.Range (1, 10);
			ani.SetInteger ("ran", ran_num);
			StartCoroutine (ran_time());
			check = 1;
		}
		if (ani.GetCurrentAnimatorStateInfo (0).IsName ("Action.Random_talk") == false) 
		{
			check = 0;
		}
	}
	private IEnumerator ran_time()
	{
		yield return new WaitForSeconds (Random.Range(2f,4f));
		ani.SetTrigger ("stop");
	}
}
