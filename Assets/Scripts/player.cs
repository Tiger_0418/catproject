﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {
    private Animator animator;
    private float current = 0;
    private float delayWeight;

    private List<string> playlist = null;
    private string nowPlayAni = "";
    private float countdown = 0.5f;

    private string[] Random_talk =
    {
        "Take 001 1",
        "Take 001 3",
        "Take 001",
        "Take 001 0",
        "think 0",
        "Take 001 5",
        "Take 001 4",
        "whisper 0",
        "Take 001 2",
        "talk 0",
        "Random"
    };
    public String actionlist = null;
    private Dictionary<String, String> actions = null;

    // Use this for initialization
    void Start () {
        playlist = new List<string>();
        animator = GetComponent<Animator>();
        actions = new Dictionary<string, string>();
        if(actionlist != null && actionlist.Length > 0)
        {
            String[] lists = actionlist.Split(new char[] { '|' });
            foreach (String single in lists)
            {
                String[] oneline = single.Split(new char[] { ',' });
                //Debug.Log(single);

                try
                {
                    actions.Add(oneline[0], oneline[1]);
                }catch(Exception e)
                {
                    Debug.Log(e.Message +":" +oneline[0]);
                }
            }
        }
    }

    // FixedUpdate is called once per frame
    void Update ()
    {
        if (playlist.Count > 0 && nowPlayAni.Length <= 0)
        {
            Debug.Log("play ani_1:" + playlist[0]);
            animator.CrossFade(playlist[0], 0, 0);
            nowPlayAni = playlist[0];
            playlist.RemoveAt(0);
            countdown = 0.5f;
        }

        if (nowPlayAni.Length > 0 && nowPlayAni.Contains("Random"))
        {
            bool ok = false;
            for (int i = 0; i < Random_talk.Length; i++)
            {
                ok = AnimatorIsPlaying(Random_talk[i]);
                if (ok)
                {
                    Debug.Log("play ani_1:" + Random_talk[i]);
                    break;
                }
            }

            countdown -= Time.deltaTime;

            if (nowPlayAni.Length > 0 && !ok && countdown <= 0.0f)
            {
                try
                {
                    //callback廠商
                    using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                    {
                        unityClass.CallStatic("callbackMessage", new object[] { "action", "finish#" + nowPlayAni });
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message + "player_Update_1");
                }
                nowPlayAni = "";
                animator.CrossFade("B0000", 0, 0);
            }
        }
        else if (nowPlayAni.Length > 0)
        {
            String ani2 = null;
            try
            {
                ani2 = actions[nowPlayAni];
            }
            catch (Exception ee)
            {
                Debug.Log(ee.Message + ":" + nowPlayAni);
            }
            bool playQB = AnimatorIsPlaying(nowPlayAni);
            bool playQB2 = false;
            if (ani2 != null && ani2.Length > 0)
                playQB2 = AnimatorIsPlaying(ani2);

            if (playQB2)
            {
                Debug.Log("play ani_2:" + ani2);
            }

            countdown -= Time.deltaTime;

            if (nowPlayAni.Length > 0 && !playQB && !playQB2 && countdown <= 0.0f)
            {
                //callback廠商
                try
                {
                    using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                    {
                        unityClass.CallStatic("callbackMessage", new object[] { "action", "finish#" + nowPlayAni });
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message + "player_Update_2");
                }
                nowPlayAni = "";
            }
        }
    }

    public void AniPlay(String ani)
    {
        playlist.Add(ani);
    }

    bool AnimatorIsPlaying(string stateName)
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
}
