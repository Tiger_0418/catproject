﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoCtl : MonoBehaviour {
    public string imgurl;
    public string youtubeurl;
    public int width;
    public int height;
    public int left, top;
    public RawImage imageToDisplay;  // This is the GameObject with the Image Component on it

    public void moveObject(string x, string y, string size)
    {
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                float xpos = float.Parse(x);
                float ypos = float.Parse(y);
                float scale = float.Parse(size);
                s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(xpos, ypos, 0);
                s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                /*
                Rect rectVideo = MainObj.GUIRectWithObject(s.gameObject);

                left = (int)rectVideo.xMin;
                top = (int)rectVideo.yMin;
                width = (int)rectVideo.width;
                height = (int)rectVideo.height;
                */
                break;
            }
        }
    }

    public void showVideo(string x, string y, string size)
    {
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                float xpos = float.Parse(x);
                float ypos = float.Parse(y);
                float scale = float.Parse(size);
                s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(xpos, ypos, 0);
                s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                s.gameObject.SetActive(true);
                /*
                Rect rectVideo = MainObj.GUIRectWithObject(s.gameObject);
                
                left = (int)rectVideo.xMin;
                top = (int)rectVideo.yMin;
                width = (int)rectVideo.width;
                height = (int)rectVideo.height;
                
                Debug.Log("left:" + left + "/top:" + top + "/width:" + width + "/height:" + height);
                */
                break;
            }
        }
        StartCoroutine(LoadImage(imgurl));
    }

    public void onVideoFinish(string data)
    {
        Debug.Log("Video played, data: " + data);
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                s.Find("video_img").gameObject.SetActive(true);
                //s.gameObject.SetActive(false);

                break;
            }
        }
    }

    public void onVideoClose(string data)
    {
        Debug.Log("Video closed, data: " + data);
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                s.Find("video_img").gameObject.SetActive(true);
                //s.gameObject.SetActive(false);

                break;
            }
        }
    }

    public void onPrepared(string data)
    {
        Debug.Log("Video prepared, data: " + data);

        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                s.gameObject.SetActive(true);
                s.Find("video_img").gameObject.SetActive(false);
                break;
            }
        }
    }

    public void onError(string data)
    {
        Debug.Log("Video onError, data: " + data);
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                s.Find("video_img").gameObject.SetActive(true);
                s.gameObject.SetActive(false);

                break;
            }
        }
    }

    private IEnumerator LoadImage(string loadedURL)
    {
        WWW www = new WWW(loadedURL);
        while (!www.isDone)
        {
            Debug.Log("Download image on progress" + www.progress);
            yield return null;
        }

        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                s.gameObject.SetActive(true);
                imageToDisplay = s.Find("video_img").GetComponentInChildren<RawImage>();
                break;
            }
        }

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Download failed");
        }
        else
        {
            Debug.Log("Download succes");
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(www.bytes);
            texture.Apply();

            imageToDisplay.texture = texture;
        }   
    }

    public void playVideo()
    {
        bool bshowed = false;
        Transform[] trs = GameObject.Find("Canvas").GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == "video_border")
            {
                bshowed = s.gameObject.activeSelf;
                break;
            }
        }

        if (bshowed)
        {
            MediaPlayerBehavior player = GetComponent<MediaPlayerBehavior>();
            if (player != null)
            {
                //Debug.Log("left:" + left + "/top:" + top + "/width:" + width + "/height:" + height);
                width = 0;
                height = 0;
                left = 0;
                top = 0;
                player.Init(youtubeurl, width, height, left, top);
            }
            else
                Debug.Log("can not find MediaPlayerBehavior player");
        }
    }
}
