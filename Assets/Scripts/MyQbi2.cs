﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MyQbi2 : MonoBehaviour {
    private bool bcontinue = true;
    private bool bStartok = false;
    public Vector3 orignalpos;

    private void Awake()
    {
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackInit", new object[] { "ok" });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "DeviceChange_OnOrientationChange");
        }
    }

    private void Start()
    {
        float widthToBeSeen = GameObject.Find("Background Image").GetComponent<SpriteRenderer>().bounds.size.x;
        Camera.main.orthographicSize = (float)(widthToBeSeen * (float)Screen.height / (float)Screen.width * 0.5f);

        Vector3 worldPoint = Camera.main.ScreenToWorldPoint(new Vector3((GameObject.Find("QB").transform.localScale.x * 4), Screen.height - (GameObject.Find("QB").transform.localScale.y * 6), 5));
        GameObject.Find("QB").transform.position = worldPoint;
        Animator ani = GameObject.Find("QB").GetComponent<Animator>();
        ani.Play("fall_once", -1, 0.1f);
    }

    private void Update()
    {
        if(GameObject.Find("QB") != null) {
            if(AnimatorIsPlaying("B0000") && !bStartok) {
                if(System.Math.Abs(GameObject.Find("QB").transform.localPosition.y-orignalpos.y) < 0.15) {
                    bStartok = true;
                    Debug.Log("start animation completed");
                    try
                    {
                        using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                        {
                            unityClass.CallStatic("callbackInitOk", new object[] { "1" });
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Log(e.Message + "MyQbi2_Update");
                    }
                }
            } else if(!bStartok) {
                GameObject.Find("QB").transform.localPosition = Vector3.Lerp(GameObject.Find("QB").transform.localPosition,
                orignalpos, 0.15f);
            }
        }        
    }

    bool AnimatorIsPlaying(string stateName)
    {
        return GameObject.Find("QB").GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(stateName);
    }
    
    [System.Serializable]
    public class Qbivideo
    {
        public string videoname;
        public string imgurl;
        public string youtubeurl;
        public string x;
        public string y;
        public string size;

        public static Qbivideo CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Qbivideo>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    [System.Serializable]
    public class Qbiphototext
    {
        public string imgurl;
        public string talks;
        public string x;
        public string y;
        public string size;

        public static Qbiphototext CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Qbiphototext>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    [System.Serializable]
    public class Qbiwrods
    {
        public string message;
        public string size;
        public string Showpos_x;
        public string Showpos_y;
        public string x;
        public string y;

        public static Qbiwrods CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Qbiwrods>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    [System.Serializable]
    public class Qbimove
    {
        public string name;
        public string size;
        public string x;
        public string y;

        public static Qbimove CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Qbimove>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }
    
    //搬移元件或放大縮小
    public void MoveComponent(string command)
    {
        //解析字串
        try
        {
            //帶入對話
            if (command.Length > 0)
            {
                var mess = Qbimove.CreateFromJSON(command);
                Debug.Log(mess.name);

                if(mess.name.Equals("Text"))
                {
                    Dialogure2 ctl = GameObject.Find("DialogCtl").GetComponent<Dialogure2>();
                    ctl.movesayObject(mess.x, mess.y, mess.size);
                }
                if (mess.name.Equals("Image"))
                {
                    Dialogure2 ctl = GameObject.Find("PhotoCtl").GetComponent<Dialogure2>();
                    ctl.moveObject(mess.x, mess.y, mess.size);
                }
                if (mess.name.Equals("Video"))
                {
                    VideoCtl ctl = GameObject.Find("VideoCtl").GetComponent<VideoCtl>();
                    ctl.moveObject(mess.x, mess.y, mess.size);
                }
                if (mess.name.Equals("Audio"))
                {
                    AudioCtl ctl = GameObject.Find("AudioCtl").GetComponent<AudioCtl>();
                    ctl.moveObject(mess.x, mess.y, mess.size);
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //圖文窗
    public void ShowPhotoText(string command)
    {
        //解析字串
        try
        {
            //帶入對話
            if (command.Length > 0)
            {
                var mess = Qbiphototext.CreateFromJSON(command);
                Debug.Log(mess.imgurl + "|" + mess.talks);
                if (mess.imgurl.Length > 0)
                {
                    Dialogure2 ctl = GameObject.Find("PhotoCtl").GetComponent<Dialogure2>();
                    ctl.showPhotoText(mess.imgurl, mess.talks, mess.x, mess.y, mess.size);

                    //StopAllCoroutines();
                    //StartCoroutine(PlayAni_start(mess.model));
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    //影音窗
    public void ShowVideo(string command)
    {
        //解析字串
        try
        {
            //帶入對話
            if (command.Length > 0)
            {
                var mess = Qbivideo.CreateFromJSON(command);
                Debug.Log(mess.imgurl + "|" + mess.youtubeurl);
                if (mess.imgurl.Length > 0 && mess.youtubeurl.Length > 0)
                {
                    VideoCtl ctl = GameObject.Find("VideoCtl").GetComponent<VideoCtl>();
                    ctl.imgurl = mess.imgurl;
                    ctl.youtubeurl = mess.youtubeurl;
                    /*
                    ctl.width = 0;
                    ctl.height = 0;
                    ctl.left = 0;
                    ctl.top = 0;
                    */
                    ctl.showVideo(mess.x, mess.y, mess.size);

                    //StopAllCoroutines();
                    //StartCoroutine(PlayAni_start(mess.model));
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    public void ShowCustomUI(string arg)
    {
        if (arg == null || arg.Length <= 0) return;
        String fullclassname = "", param = "";
        String[] comm = arg.Split(new char[] { '&' });
        for (int i = 0; i < comm.Length; i++)
        {
            String[] single = comm[i].Split(new char[] { '=' });
            if (single[0].Equals("c"))
            {
                fullclassname = single[1];
            }
            if (single[0].Equals("p"))
            {
                param = single[1];
            }
        }
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass(fullclassname))
            {
                unityClass.CallStatic("ShowCustomUI", new object[] { param });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "ShowCustomUI");
        }
    }

    //純文字窗
    public void InputFromAgent(string command)
    {
        //解析字串
        try
        {
            //帶入對話
            if (command.Length > 0)
            {
                var mess = Qbiwrods.CreateFromJSON(command);
                Debug.Log(mess.message);

                if (mess.message.Length > 0)
                {
                    mess.message = mess.message.Replace("[link action='", "<link=\"action=");
                    mess.message = mess.message.Replace("[link submit='", "<link=\"submit=");
                    mess.message = mess.message.Replace("[link url='", "<link=\"url=");
                    mess.message = mess.message.Replace("']", "\">");
                    mess.message = mess.message.Replace("[/link]", "</link>");
                    Debug.Log("text:" + mess.message);
                    GameObject.Find("DialogCtl").GetComponent<Dialogure2>().say(mess.message, mess.size, mess.Showpos_x, mess.Showpos_y, mess.x, mess.y);
                }

                //StopAllCoroutines();
                //StartCoroutine(PlayAni_start(mess.model));
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }
    
    public void ShowActions(string actions)
    {
        if (actions.Length <= 0) return;

        StopAllCoroutines();
        StartCoroutine(PlayAni_start(actions));
    }

    IEnumerator PlayAni_start(string ani_name)
    {
        GameObject.Find("QB").GetComponent<player>().AniPlay(ani_name);
        yield return new WaitForSeconds(0.1f);
    }

    public void stopAni()
    {
        StopAllCoroutines();
    }
}
