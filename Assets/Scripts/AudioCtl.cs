﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class AudioCtl : MonoBehaviour {
    private Text curr = null;
    private Text total = null;
    private Progress progress = null;
    public Vector3 orgvec;

    public void moveObject(string x, string y, string size)
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform ts in trs)
        {
            if (ts.name == "border")
            {
                float posx = 0, posy = 0, scale = 0;
                posx = float.Parse(x);
                posy = float.Parse(y);
                scale = float.Parse(size);

                ts.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                ts.GetComponent<RectTransform>().localPosition = new Vector3(posx, posy, 0);
                break;
            }
        }
    }

    public void showAudio(string param)
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach(Transform ts in trs)
        {
            if(ts.name == "border")
            {
                float x = 0, y = 0, scale = 0;
                string [] para = param.Split(new char[] { '&' });
                for(int i = 0; i < para.Length; i++)
                {
                    string [] one = para[i].Split(new char[] { '=' });
                    if(one[0].Equals("x"))
                    {
                        x = float.Parse(one[1]);
                    }
                    if (one[0].Equals("y"))
                    {
                        y = float.Parse(one[1]);
                    }
                    if (one[0].Equals("size"))
                    {
                        scale = float.Parse(one[1]);
                    }
                }
                ts.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                ts.GetComponent<RectTransform>().localPosition = new Vector3(x, y, 0);
                ts.gameObject.SetActive(true);
                curr = ts.Find("curr").GetComponent<Text>();
                curr.transform.localPosition = orgvec;
                total = ts.Find("total").GetComponent<Text>();
                progress = ts.Find("progress").GetComponent<Progress>();
                progress.Value = 0;
                Image imgbtn = ts.Find("play_pause").gameObject.GetComponent<Image>();
                imgbtn.sprite = Resources.Load <Sprite> ("play");
                imgbtn.sprite.name = "play";
                break;
            }
        }
    }

    public void showAudio2()
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach(Transform ts in trs)
        {
            if(ts.name == "border")
            {
                ts.gameObject.SetActive(true);
                curr = ts.Find("curr").GetComponent<Text>();
                curr.transform.localPosition = orgvec;
                total = ts.Find("total").GetComponent<Text>();
                progress = ts.Find("progress").GetComponent<Progress>();
                progress.Value = 0;
                break;
            }
        }
        changeIcon();
    }

    public void setAudio(string command)
    {
        string[] ans = command.Split(new char[] { '|' });

        string currtime = ans[0];
        string totaltime = ans[1];
        int progressvalue = int.Parse(ans[2]);

        curr.text = currtime;
        total.text = totaltime;

        //float TranslateSpeed = 10f;
        //curr.transform.Translate(Vector3.forward * TranslateSpeed);
        progress.Value = progressvalue;
    }

    public void hideAudio()
    {
        Transform[] trs = this.transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform ts in trs)
        {
            if (ts.name == "border")
            {
                curr.text = "";
                total.text = "";
                progress.Value = 0;
                ts.gameObject.SetActive(false);
               
                break;
            }
        }
    }

    public int changeIcon() {
        Image imgbtn = this.transform.Find("border").Find("play_pause").gameObject.GetComponent<Image>();
        if(imgbtn.sprite.name == "pause") 
        {
            Debug.Log("pause audio");
            imgbtn.sprite = Resources.Load <Sprite> ("play");
            imgbtn.sprite.name = "play";
            return 1;
        }
        else
        {
            Debug.Log("resume audio");
            imgbtn.sprite = Resources.Load <Sprite> ("pause");
            imgbtn.sprite.name = "pause";
            return 2;
        }
    }
}
