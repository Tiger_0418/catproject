﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class MainObj : MonoBehaviour
{
    public static Rect GUIRectWithObject(GameObject go)
    {
        Vector2 screenPos = Camera.main.WorldToViewportPoint(go.GetComponent<RectTransform>().position);
        Rect rectThis = RectTransformUtility.PixelAdjustRect(go.GetComponent<RectTransform>(), go.GetComponentInParent<Canvas>());
        //return new Rect(screenPos.x, screenPos.y, 
        //    go.GetComponent<RectTransform>().rect.width, go.GetComponent<RectTransform>().rect.height);
        return rectThis;
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log(Screen.width);
        DontDestroyOnLoad(this);
        //StartCoroutine(CheckForChange());
        //StartCoroutine(testchangebackimg());
    }

    public void Initial(string command)
    {
        
        string[] p = command.Split(new char[] { '|' });
        int oreitation = int.Parse(p[0]);
        
        if (oreitation == 0)
        {
            Screen.orientation = ScreenOrientation.Landscape;
            StartCoroutine(LoadScene(2));
        }
        else
        {
            Screen.orientation = ScreenOrientation.Portrait;
            StartCoroutine(LoadScene(1));
        }
    }

    IEnumerator LoadScene(int scene)
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(scene);
    }

    IEnumerator testchangebackimg()
    {
        yield return new WaitForSeconds(5.0f);

        Debug.Log("check testchangebackimg()");
        SetBackgroundImage("{\"imgpath\":\"https://thewallpaper.co/wp-content/uploads/2016/10/1080-x-1920-Backgrounds-Free-Download-hd-desktop-wallpapers-cool-images-hd-download-windows-colourfull-display-lovely-wallpapers-1080x1920.jpg\",\"localsite\":false}");
    }

    [System.Serializable]
    public class QbibackImg
    {
        public string imgpath;
        public bool localsite;

        public static QbibackImg CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<QbibackImg>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    public void SetBackgroundImage(string command)
    {
        if (command.Length > 0)
        {
            var data = QbibackImg.CreateFromJSON(command);
            Debug.Log(data.imgpath + "|" + data.localsite);

            if (!data.localsite)
            {
                StartCoroutine(LoadBackImage(data.imgpath));
            }
            else
            {
                Texture2D texture = null;
                byte[] fileData;

                if (File.Exists(data.imgpath))
                {
                    fileData = File.ReadAllBytes(data.imgpath);
                    texture = new Texture2D(1, 1);
                    texture.LoadImage(fileData); //..this will auto-resize the texture dimensions.

                    Rect orignrect = GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite.rect;
                    GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite =
                        Sprite.Create(texture, orignrect, Vector2.zero);
                    //GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
                    float widthToBeSeen = GameObject.Find("Background Image").GetComponent<SpriteRenderer>().bounds.size.x;
                    Camera.main.orthographicSize = (float)(widthToBeSeen * (float)Screen.height / (float)Screen.width * 0.5f);

                    if (orignrect.width == 1280 && orignrect.height == 800)
                        GameObject.Find("Background Image").transform.localPosition = new Vector3(1.31f, -2.9f, -6.090906f);
                    else if (orignrect.width == 1920 && orignrect.height == 1080)
                        GameObject.Find("Background Image").transform.localPosition = new Vector3(4.5f, -4.3f, -6.090906f);
                    else if (orignrect.width == 1080 && orignrect.height == 1920)
                        GameObject.Find("Background Image").transform.localPosition = new Vector3(-10.5f, -8.4f, -6.090906f);

                    Debug.Log("Background Image changed");
                }
                else
                    Debug.Log("file not found:" + data.imgpath);
            }
        }
    }

    private IEnumerator LoadBackImage(string url)
    {
        Debug.Log("start download backimg:" + url);
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            Debug.Log("Download image on progress" + www.progress);
            yield return null;
        }

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Download failed");
        }
        else
        {
            Debug.Log("Download succes");
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(www.bytes);
            texture.Apply();

            Rect orignrect = GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite.rect;
            GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite = 
                Sprite.Create(texture, orignrect, Vector2.zero);
            //GameObject.Find("Background Image").GetComponent<SpriteRenderer>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
            float widthToBeSeen = GameObject.Find("Background Image").GetComponent<SpriteRenderer>().bounds.size.x;
            Camera.main.orthographicSize = (float)(widthToBeSeen * (float)Screen.height / (float)Screen.width * 0.5f);

            if (orignrect.width == 1280 && orignrect.height == 800)
                GameObject.Find("Background Image").transform.localPosition = new Vector3(1.31f, -2.9f, -6.090906f);
            else if (orignrect.width == 1920 && orignrect.height == 1080)
                GameObject.Find("Background Image").transform.localPosition = new Vector3(4.5f, -4.3f, -6.090906f);
            else if (orignrect.width == 1080 && orignrect.height == 1920)
                GameObject.Find("Background Image").transform.localPosition = new Vector3(-10.5f, -8.4f, -6.090906f);

            Debug.Log("Background Image changed");
        }
    }

    [System.Serializable]
    public class Qbimove
    {
        public string name;
        public string size;
        public string x;
        public string y;
        public string btn;

        public static Qbimove CreateFromJSON(string jsonString)
        {
            return JsonUtility.FromJson<Qbimove>(jsonString);
        }

        // Given JSON input:
        // {"name":"Dr Charles","lives":3,"health":0.8}
        // this example will return a PlayerInfo object with
        // name == "Dr Charles", lives == 3, and health == 0.8f.
    }

    public void moveBtn(string command)
    {
        //解析字串
        try
        {
            //帶入對話
            if (command.Length > 0)
            {
                var mess = Qbimove.CreateFromJSON(command);
                Debug.Log(mess.name);

                if (mess.name.Equals("IdleButton"))
                {
                    float posx = float.Parse(mess.x);
                    float posy = float.Parse(mess.y);
                    float scale = float.Parse(mess.size);

                    Transform[] trs = GameObject.Find("Canvas").transform.GetComponentsInChildren<Transform>(true);
                    foreach (Transform s in trs)
                    {
                        if (s.name == mess.btn)
                        {
                            s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(posx, posy, 0);
                            s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.ToString());
        }
    }

    public void setBtn(string command)
    {
        string objname, url,size,x,y;
        objname = command.Split(new char[] { '|' })[0];
        url = command.Split(new char[] { '|' })[1];
        size = command.Split(new char[] { '|' })[2];
        x = command.Split(new char[] { '|' })[3];
        y = command.Split(new char[] { '|' })[4];
        float scale = float.Parse(size);
        float posx = float.Parse(x);
        float posy = float.Parse(y);

        Transform[] trs = GameObject.Find("Canvas").transform.GetComponentsInChildren<Transform>(true);
        foreach (Transform s in trs)
        {
            if (s.name == objname)
            {
                s.gameObject.GetComponent<RectTransform>().localPosition = new Vector3(posx, posy, 0);
                s.gameObject.GetComponent<RectTransform>().localScale = new Vector3(scale, scale, scale);
                break;
            }
        }
        StartCoroutine(LoadImage(objname, url));
    }

    private IEnumerator LoadImage(string objname, string url)
    {
        Debug.Log("start download btn:" + objname + ",url:" + url);
        WWW www = new WWW(url);
        while (!www.isDone)
        {
            Debug.Log("Download image on progress" + www.progress);
            yield return null;
        }

        if (!string.IsNullOrEmpty(www.error))
        {
            Debug.Log("Download failed");
        }
        else
        {
            Debug.Log("Download succes");
            Texture2D texture = new Texture2D(1, 1);
            texture.LoadImage(www.bytes);
            texture.Apply();

            Transform[] trs = GameObject.Find("Canvas").transform.GetComponentsInChildren<Transform>(true);
            foreach (Transform s in trs)
            {
                if (s.name == objname)
                {
                    s.gameObject.SetActive(true);
                    s.gameObject.GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height),
Vector2.zero);
                    Debug.Log("Button enabled:" + s.name);
                    break;
                }
            }
        }
    }
}
