﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using TMPro;
using System;

[RequireComponent(typeof(TextMeshProUGUI))]
public class Textfad : MonoBehaviour, IPointerClickHandler
{
    // UI.Text
    public TextMeshProUGUI m_text;
    private Canvas canvas;
    private Camera camera;

    private string m_content;
    public Vector3 showpos;
    public Vector3 showscale;
    public float m_timer = 0.2f;
    public float m_timer2 = 0.5f;
    public int pagemaxlen = 90;
    public int pagemaxLine = 10;
    private int linecount = 0;

    //全部有幾頁
    private int totalpages = 0;
    //目前在第幾頁
    private int currentpage = 0;

    private float m_fade = 2.0f;
    // Has to be min 0.1f or new word blinks one time (I don't know why)
    private float m_colorFloat = 0.1f;
    private int m_colorInt = 0;
    private int m_counter = 0, m_counter2 = 0;
    private string m_show = "";
    private char[] m_wordArray = null;
    private float nowScaleSize = 0.01f;
    private int showclose = 0;
    private Vector3 orignpos;
    private bool bStartTalk = false;
    private bool bDontClose = false;
    private bool bChangepage = false;

    private void Awake()
    {
        orignpos = GetComponent<RectTransform>().localPosition;
        GameObject[] objects = SceneManager.GetActiveScene().GetRootGameObjects();
        for (int i = 0; i < objects.Length; i++)
        {
            if (objects[i].name == "Qbi-port")
            {
                canvas = objects[i].transform.Find("Canvas").GetComponent<Canvas>();
                break;
            }
        }
              
        if (canvas.renderMode == RenderMode.ScreenSpaceOverlay)
        {
            camera = null;
        }
        else
        {
            camera = canvas.worldCamera;
        }
    }

    public void clear()
    {
        Debug.Log("clear");
        m_wordArray = null;
        m_content = null;
        m_text.text = "";
        bStartTalk = false;
        m_show = "";
        showclose = 2;
    }

    public void showphototext(string conversation, float textspeed, float pagespeed)
    {
        if (conversation == null || conversation.Length <= 0) return;

        if (!bStartTalk)
        {
            //GameObject.Find("QB").GetComponent<player>().AniPlay(talk_model);
            bStartTalk = true;
        }

        m_timer = textspeed;
        m_timer2 = pagespeed;

        //Debug.Log("say:" + conversation);
        m_content = conversation;
        m_counter = 0;
        m_counter2 = 0;
        bDontClose = true;

        Play();
    }

    public void showDialog(string conversation, float textspeed, float pagespeed)
    {
        if (conversation == null || conversation.Length <= 0) return;

        if (!bStartTalk)
        {
            //GameObject.Find("QB").GetComponent<player>().AniPlay(talk_model);
            bStartTalk = true;
        }
        m_timer = textspeed;
        m_timer2 = pagespeed;

        Debug.Log("say:" + conversation);
        m_content = conversation;
        GetComponent<RawImage>().enabled = true;
        showclose = 1;
        m_counter = 0;
        m_counter2 = 0;
    }

    private void Play()
    {
        Debug.Log("start wording: " + m_content);
        string m_temp_content = m_content;
        while (m_temp_content.IndexOf("<link") >= 0)
        {
            int start = m_temp_content.IndexOf("<link");
            int end = m_temp_content.IndexOf(">", start + 1);
            int start2 = m_temp_content.IndexOf("</link>", end + 1);

            m_temp_content = m_temp_content.Remove(start2, 7);
            m_temp_content = m_temp_content.Remove(start, end - start + 1);
        }
        int totallens = m_temp_content.ToCharArray().Length;
        m_wordArray = m_content.ToCharArray();  //直接用全部了

        totalpages = (totallens / pagemaxlen) +
    ((totallens % pagemaxlen != 0) ? 1 : 0);
        currentpage = 0;
    }

    private void Update()
    {
        if (showclose == 1)
        {
             Vector3 newScale = showscale;
             Vector3 newPos = showpos;
            GetComponent<RectTransform>().localScale = Vector3.Lerp(GetComponent<RectTransform>().localScale,
        newScale, 0.1f);
            GetComponent<RectTransform>().localPosition = Vector3.Lerp(GetComponent<RectTransform>().localPosition,
                newPos, 0.1f);

            if (GetComponent<RectTransform>().localScale.x > (newScale.x - 0.1f))
            {
                Play();
                showclose = 0;
            }
        }

        if(showclose == 2)
        {
            Vector3 newScale = new Vector3(0.01f, 0.01f, 0.01f);
            Vector3 newPos = new Vector3(0, -99.9f, 0);
            GetComponent<RectTransform>().localScale = Vector3.Lerp(GetComponent<RectTransform>().localScale,
        newScale, 0.3f);
            GetComponent<RectTransform>().localPosition = Vector3.Lerp(GetComponent<RectTransform>().localPosition,
                orignpos, 0.5f);

            if (GetComponent<RectTransform>().localScale.x < 0.1f)
            {
                GetComponent<RawImage>().enabled = false;
                showclose = 0;
                m_content = null;
                Debug.Log("end it");
                this.gameObject.SetActive(false);

                if (bStartTalk)
                {
                    bStartTalk = false;
                }
            }
        }

        if (m_wordArray != null)
        {
            if (m_counter2 < pagemaxlen && m_counter < m_wordArray.Length
                && linecount <= pagemaxLine)
            {
                if (m_colorFloat <= m_timer)
                {
                    if (m_wordArray[m_counter] == '<')
                    {
                        if (m_wordArray[m_counter + 1] == 'l')
                        {
                            string m_findText = new string(m_wordArray);
                            m_colorFloat = 0.1f;
                            int findend = m_findText.IndexOf("</link>", m_counter + 1);
                            int findstart = m_findText.IndexOf(">", m_counter + 1);
                            int lens = findend - findstart - 1;
                            Debug.Log("add lens:" + lens);
                            m_counter2 += lens;
                            findend += 7;
                            string link =
                                m_findText.Substring(m_counter, findend - m_counter);
                            Debug.Log("m_show:" + m_show);
                            m_show = (m_show + "<#ec895d><u><b>" + link + "</b></u></color>");
                            Debug.Log("change m_show:" + m_show);
                            m_text.text = m_show;
                            Debug.Log("m_text:" + m_text.text);
                            m_counter = findend;
                            Debug.Log("m_counter:" + m_counter + ",arrlen:" + m_wordArray.Length);

                            linecount = m_text.textInfo.lineCount;
                            for (int i = 0; i < m_text.textInfo.lineInfo.Length; i++)
                            {
                                Debug.Log("line " + i + "words:" + m_text.textInfo.lineInfo[i].characterCount);
                                if ((m_text.textInfo.lineInfo[i].characterCount * 35) / m_text.textInfo.lineInfo[i].width > 0)
                                    linecount += (int)((m_text.textInfo.lineInfo[i].characterCount * 35) / m_text.textInfo.lineInfo[i].width);
                            }
                            Debug.Log("lines count:" + linecount);
                            if (m_counter >= m_wordArray.Length)
                            {
                                m_wordArray = null;
                                m_counter = 0;
                                m_counter2 = 0;
                                return;
                            }
                            return;
                        }
                    }

                    m_colorFloat += Time.deltaTime / m_fade;
                    m_colorInt = (int)(Mathf.Lerp(0.0f, 1.0f, m_colorFloat) * 255.0f);
                    m_text.text = m_show + ("<color=#FFFFFF" + string.Format("{0:X}", m_colorInt) + ">" + m_wordArray[m_counter] + "</color>");
                    //Debug.Log(("<color=\"#FFFFFF" + string.Format("{0:X}", m_colorInt) + "\">" + m_wordArray[m_counter] + "</color>"));
                }
                else
                {
                    m_colorFloat = 0.1f;
                    m_show += m_wordArray[m_counter] + "";
                    m_counter++;
                    m_counter2++;
                    Debug.Log("m_counter:" + m_counter + ",arrlen:" + m_wordArray.Length);
                    if (m_counter == m_wordArray.Length)
                    {
                        int end = m_text.text.LastIndexOf("<color");
                        m_text.text = m_text.text.Remove(end, m_text.text.Length - end);
                        m_text.text += m_wordArray[m_counter - 1];
                    }

                    linecount = m_text.textInfo.lineCount;
                    for (int i = 0; i < m_text.textInfo.lineInfo.Length; i++)
                    {
                        Debug.Log("line " + i + "words:" + m_text.textInfo.lineInfo[i].characterCount);
                        if ((m_text.textInfo.lineInfo[i].characterCount * 35) / m_text.textInfo.lineInfo[i].width > 0)
                            linecount += (int)((m_text.textInfo.lineInfo[i].characterCount * 35) / m_text.textInfo.lineInfo[i].width);
                    }
                    Debug.Log("lines count:" + linecount);
                    if (m_counter >= m_wordArray.Length)
                    {
                        m_wordArray = null;
                        m_counter = 0;
                        m_counter2 = 0;
                        return;
                    }
                }
            }
            else
            {
                /*
                int indexofcolor = m_text.text.IndexOf("<color");
                if (indexofcolor >= 0)
                {
                    string temptext = m_text.text.Substring(m_text.text.IndexOf(">")+1,
                        m_text.text.IndexOf("</color>") - m_text.text.IndexOf(">")-1);
                    if (temptext.Length > 0)
                    {
                        //Debug.Log(temptext);
                        m_text.text = m_text.text.Remove(indexofcolor, m_text.text.Length - indexofcolor);
                        //Debug.Log(m_text.text);
                        m_text.text += temptext;
                    }
                    m_wordArray = null;
                    m_show = "";

                    currentpage++;
                    StartCoroutine(changePage());
                }
                */
                if (!bChangepage)
                {
                    currentpage++;

                    if (currentpage >= totalpages)
                    {
                        if (m_counter >= m_wordArray.Length)
                        {
                            m_wordArray = null;
                            m_counter = 0;
                            m_counter2 = 0;
                            return;
                        }
                        else
                        {
                            m_text.text = "";
                            linecount = 0;
                        }
                    }

                    bChangepage = true;
                    char[] temp = new char[m_wordArray.Length - m_counter];
                    for (int i = m_counter; i < m_wordArray.Length; i++)
                    {
                        temp[i - m_counter] = m_wordArray[i];
                    }
                    m_wordArray = temp;
                    string nn = new string(m_wordArray);
                    Debug.Log("remain:" + nn);
                    m_show = "";

                    StartCoroutine(changePage());
                }
            }
        }
    }

    private IEnumerator changePage()
    {
        yield return new WaitForSeconds(m_timer2);

        m_counter = 0;
        m_counter2 = 0;
        bChangepage = false;
        /*
        if (currentpage == totalpages)
        {
            //if (!bDontClose)
            //    showclose = 2;
        }
        else
        {
            m_text.text = "";   //清空重來
            char[] temp = m_content.ToCharArray();
            int total = temp.Length - (currentpage * pagemaxlen);
            if (total > pagemaxlen)
                total = pagemaxlen;

            char[] words = new char[total];


            for (int i = currentpage * pagemaxlen; i < (total + (currentpage * pagemaxlen)); i++)
            {
                //Debug.Log("innn");
                words[i % pagemaxlen] = temp[i];
            }

            m_counter = 0;
            m_wordArray = words;
        }
        */
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(m_text, Input.mousePosition, camera);
        if (linkIndex != -1)
        {
            TMP_LinkInfo linkInfo = m_text.textInfo.linkInfo[linkIndex];
            try
            {
                using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                {
                    Debug.Log("callback:" + linkInfo.GetLinkID());
                    unityClass.CallStatic("callbackLink", new object[] { linkInfo.GetLinkID() });
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message + "OnPointerClick");
            }
        }
    }
}
