﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour {

    public void play_wave()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"wave_once\"]}");
    }
    public void play_bow()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!大家都愛吃漢堡保雞鴨魚肉!\",\"model\":[\"bow_once\"]}");
    }
    public void play_right()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"不吃葡萄不吐葡萄皮吃吃喝喝幹嘛要這樣子阿打打電動看看電視哈拉影城打工念書洗澡吃飯看電影我的老天鵝阿 看到哪邊了阿\",\"model\":[\"right_start\"]}");
    }
    public void play_left()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"left_start\"]}");
    }
    public void play_repeat()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"repeat_start\"]}");
    }
    public void play_talk1()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"talk_1_start\"]}");
    }
    public void play_talk2()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"talk_2_start\"]}");
    }
    public void play_talk3()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"talk_3_start\"]}");
    }
    public void play_idle()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.InputFromAgent("{\"message\":\"Hi\",\"model\":[\"QB_B000_idle\"]}");
    }

    public void showphoto()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.ShowPhotoText("{\"model\":[\"talk_1_start\"],\"imgurl\":\"https://image.gameflier.com/PB/2018092003.jpg\",\"talks\":\"\"}");
    }

    public void playvideo()
    {
        MyQbi myqb = GameObject.Find("Qbi").GetComponent<MyQbi>();
        myqb.ShowVideo("{\"model\":[\"talk_1_start\"],\"videoname\":\"\",\"imgurl\":\"https://image.gameflier.com/PB/2018092003.jpg\",\"youtubeurl\":\"https://qbi.chainsea.com.tw/gateway/custom/videoplayback.mp4\"}");
    }
}
