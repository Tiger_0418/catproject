﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class room : MonoBehaviour {

	public GameObject floor;
	public GameObject Qbi;
	public GameObject Qbi_model;
	public Texture old;
	public Texture org;

	public float time = 0.0f;

	Vector3 floor_pos;
	Vector3 Qbi_pos;
	Vector3 floor_scale;
	Vector3 Qbi_scale;

	public int roomopen_in = 0;

	int timecheck = 0;

	public float Qbi_move_x;
	public float Qbi_move_y;
	public float floor_move_x;
	public float floor_move_y;

    public float Qbi_scale_size = 30;
    public float floor_scale_size = 0.25f;

    public float Qbi_move_x2;
    public float Qbi_move_y2;
    public float floor_move_x2;
    public float floor_move_y2;

    private bool done = false;

	public bool check = false;

	public GameObject ob_weather;
	public GameObject ob_train;
	public GameObject ob_movie;
	public GameObject ob_jokes;
	public GameObject ob_fortune;
	public GameObject ob_old;
	public GameObject ob_bye;
	public GameObject ob_computer;
	public GameObject ob_newspaper;
	public GameObject ob_crash;
	public GameObject ob_news;
	public GameObject ob_cry;
	public GameObject ob_kiss;
	public GameObject ob_teeth;
	public GameObject ob_book;
	public GameObject ob_toys;
	public GameObject ob_sade;
	public GameObject ob_dementia;
	public GameObject ob_decadent;
	public GameObject ob_hello;
	public GameObject ob_ignore;
	public GameObject ob_funny;


	Animator Qbi_ani;
	int bron_1 = 0;

	// Use this for initialization
	void Start () 
	{
		floor_pos = floor.transform.localPosition;
		Qbi_pos = Qbi.transform.localPosition;
		floor_scale = floor.transform.localScale;
		Qbi_scale = Qbi.transform.localScale;

		Qbi_ani = Qbi.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		//偵測動畫名稱顯示道具
		show_item ("weather" , ob_weather);
		show_item ("train" , ob_train);
		show_item ("movie" , ob_movie);
		show_item ("jokes" , ob_jokes);
		show_item ("fortune" , ob_fortune);
		show_item ("old" , ob_old);
		show_item ("bye" , ob_bye);
		show_item ("computer" , ob_computer);
		show_item ("newspaper" , ob_newspaper);
		show_item ("crash" , ob_crash);
		show_item ("news" , ob_news);
		show_item ("cry" , ob_cry);
		show_item ("kiss" , ob_kiss);
		show_item ("teeth" , ob_teeth);
		show_item ("book" , ob_book);
		show_item ("toys" , ob_toys);
		show_item ("sade" , ob_sade);
		show_item ("dementia" , ob_dementia);
		show_item ("decadent" , ob_decadent);
		show_item ("hello" , ob_hello);
		show_item ("ignore" , ob_ignore);
		show_item ("funny" , ob_funny);

		//old動畫換貼圖
		if (Qbi_ani.GetCurrentAnimatorStateInfo (0).IsName ("old")) 
		{
			Qbi_model.GetComponent<SkinnedMeshRenderer> ().material.mainTexture = old;
		} 
		else 
		{
			Qbi_model.GetComponent<SkinnedMeshRenderer> ().material.mainTexture = org;
		}
			

		if (Input.GetKeyDown(KeyCode.A))
		{
            room_in ();
		}
		if (Input.GetKeyDown(KeyCode.S))
		{
            room_out ();
		}
		if (roomopen_in == 1) 
		{
			scale_in ();
		} 
		if (roomopen_in == 2) 
		{
			scale_out ();
		}
        if (roomopen_in == 3)
        {
            scale_in2();
        }
        if (roomopen_in == 4)
        {
            scale_out2();
        }
        if (time >= 2 || time == 0)
		{
			check = true;
		}
		else
		{
			check = false;
		}
	}
    public void testroom2()
    {
        room_in2("30|0.25|-8|0|-8|0");
    }

    public void room_in2(string arg)
    {
        Debug.Log("room_in2:" + arg);
        string[] comands = arg.Split(new char[] { '|' });
        Qbi_scale_size = float.Parse(comands[0]);
        floor_scale_size = float.Parse(comands[1]);
        Qbi_move_x2 = float.Parse(comands[2]);
        Qbi_move_y2 = float.Parse(comands[3]);
        floor_move_x2 = float.Parse(comands[4]);
        floor_move_y2 = float.Parse(comands[5]);

        //if (roomopen_in == 0 || roomopen_in == 3 || roomopen_in == 4)
        {
            //Qbi.GetComponent<Animator>().Play("note");
            //Qbi_pos = Qbi.transform.localPosition;
            floor_pos = floor.transform.localPosition;
            Qbi_pos = Qbi.transform.localPosition;
            floor_scale = floor.transform.localScale;
            Qbi_scale = Qbi.transform.localScale;
            time_reset();
            roomopen_in = 3;
        }
    }
    public void room_out2(string arg)
    {
        Debug.Log("room_out2:" + arg);
        string[] comands = arg.Split(new char[] { '|' });
        Qbi_scale_size = float.Parse(comands[0]);
        floor_scale_size = float.Parse(comands[1]);
        Qbi_move_x2 = float.Parse(comands[2]);
        Qbi_move_y2 = float.Parse(comands[3]);
        floor_move_x2 = float.Parse(comands[4]);
        floor_move_y2 = float.Parse(comands[5]);

        //if (roomopen_in == 0 || roomopen_in == 3 || roomopen_in == 4)
        {
            //Qbi.GetComponent<Animator>().Play("note");
            floor_pos = floor.transform.localPosition;
            Qbi_pos = Qbi.transform.localPosition;
            floor_scale = floor.transform.localScale;
            Qbi_scale = Qbi.transform.localScale;
            roomopen_in = 4;
            time_reset();
        }
    }
    void scale_in2()
    {
        if (time <= 2)
        {
            time += 1f * Time.deltaTime;
        }
        else
            roomopen_in = 0;
        Qbi.transform.localScale = new Vector3(Mathf.Lerp(Qbi_scale.x, Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.y, Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.z, Qbi_scale_size, time));
        Qbi.transform.localPosition = new Vector3(Mathf.Lerp(Qbi_pos.x, Qbi_move_x2, time), Mathf.Lerp(Qbi_pos.y, Qbi_move_y2, time), Qbi.transform.localPosition.z);
        floor.transform.localScale = new Vector3(Mathf.Lerp(floor_scale.x, floor_scale_size, time), Mathf.Lerp(floor_scale.y, floor_scale_size, time), transform.localScale.z);
        floor.transform.localPosition = new Vector3(Mathf.Lerp(floor_pos.x, floor_move_x2, time), Mathf.Lerp(floor_pos.y, floor_move_y2, time), floor.transform.localPosition.z);

        //Qbi.transform.localScale = new Vector3(Mathf.Lerp(Qbi_scale.x, Qbi_scale.x - Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.y, Qbi_scale.y - Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.z, Qbi_scale.z - Qbi_scale_size, time));
        //Qbi.transform.localPosition = new Vector3(Mathf.Lerp(Qbi_pos.x, Qbi_pos.x + Qbi_move_x2, time), Mathf.Lerp(Qbi_pos.y, Qbi_pos.y - Qbi_move_y2, time), Qbi.transform.localPosition.z);
        //floor.transform.localScale = new Vector3(Mathf.Lerp(floor_scale.x, floor_scale.x - floor_scale_size, time), Mathf.Lerp(floor_scale.y, floor_scale.y - floor_scale_size, time), transform.localScale.z);
        //floor.transform.localPosition = new Vector3(Mathf.Lerp(floor_pos.x, floor_pos.x + floor_move_x2, time), Mathf.Lerp(floor_pos.y, floor_pos.y - floor_move_y2, time), floor.transform.localPosition.z);
    }
    void scale_out2()
    {
        if (time <= 2)
        {
            time += 1f * Time.deltaTime;
        }
        else
            roomopen_in = 0;
        Qbi.transform.localScale = new Vector3(Mathf.Lerp(Qbi_scale.x, Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.y, Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.z, Qbi_scale_size, time));
        Qbi.transform.localPosition = new Vector3(Mathf.Lerp(Qbi_pos.x, Qbi_move_x2, time), Mathf.Lerp(Qbi_pos.y, Qbi_move_y2, time), Qbi.transform.localPosition.z);
        floor.transform.localScale = new Vector3(Mathf.Lerp(floor_scale.x, floor_scale_size, time), Mathf.Lerp(floor_scale.y, floor_scale_size, time), transform.localScale.z);
        floor.transform.localPosition = new Vector3(Mathf.Lerp(floor_pos.x, floor_move_x2, time), Mathf.Lerp(floor_pos.y, floor_move_y2, time), floor.transform.localPosition.z);
        //Qbi.transform.localScale = new Vector3(Mathf.Lerp(Qbi_scale.x, Qbi_scale.x + Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.y, Qbi_scale.y + Qbi_scale_size, time), Mathf.Lerp(Qbi_scale.z, Qbi_scale.z + Qbi_scale_size, time));
        //Qbi.transform.localPosition = new Vector3(Mathf.Lerp(Qbi_pos.x, Qbi_pos.x + Qbi_move_x2, time), Mathf.Lerp(Qbi_pos.y, Qbi_pos.y - Qbi_move_y2, time), Qbi.transform.localPosition.z);
        //floor.transform.localScale = new Vector3(Mathf.Lerp(floor_scale.x, floor_scale.x + floor_scale_size, time), Mathf.Lerp(floor_scale.y, floor_scale.y + floor_scale_size, time), transform.localScale.z);
        //floor.transform.localPosition = new Vector3(Mathf.Lerp(floor_pos.x, floor_pos.x + floor_move_x2, time), Mathf.Lerp(floor_pos.y, floor_pos.y - floor_move_y2, time), floor.transform.localPosition.z);
    }
    public void RoomIn()
    {
        room_in();
    }
    public void RoomOut()
    {
        room_out();
    }
    void room_in()
    {
        if (check && roomopen_in == 2)
        {
            Qbi.GetComponent<Animator>().Play("note");
            //Qbi_pos = Qbi.transform.localPosition;
            floor_pos = floor.transform.localPosition;
            Qbi_pos = Qbi.transform.localPosition;
            floor_scale = floor.transform.localScale;
            Qbi_scale = Qbi.transform.localScale;
            time_reset();
            done = false;
            roomopen_in = 1;
        }
        if (check && roomopen_in == 0)
        {
            Qbi.GetComponent<Animator>().Play("note");
            //Qbi_pos = Qbi.transform.localPosition;
            floor_pos = floor.transform.localPosition;
            Qbi_pos = Qbi.transform.localPosition;
            floor_scale = floor.transform.localScale;
            Qbi_scale = Qbi.transform.localScale;
            time_reset();
            done = false;
            roomopen_in = 1;
        }
    }
    void room_out()
    {
        if (check && roomopen_in == 1)
        {
            Qbi.GetComponent<Animator>().Play("note");
            floor_pos = floor.transform.localPosition;
            Qbi_pos = Qbi.transform.localPosition;
            floor_scale = floor.transform.localScale;
            Qbi_scale = Qbi.transform.localScale;
            roomopen_in = 2;
            try
            {
                using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                {
                    unityClass.CallStatic("hideIdleMenu");
                }
            }
            catch (Exception e)
            {
                Debug.Log(e.Message + "room_out");
            }
            //好了 可以了 關閉
            Transform[] trs = GameObject.Find("Canvas").transform.GetComponentsInChildren<Transform>(true);
            foreach (Transform tr in trs)
            {
                if (tr.name == "IdleMenu")
                {
                    tr.gameObject.SetActive(false);
                    break;
                }
            }
            time_reset();
        }
    }
    void scale_in () 
	{
		if (time <= 2)
		{
            time += 1f * Time.deltaTime;
		}
        else
        {
            if (!done)
            {
                done = true;
                //好了 可以了 顯示
                Transform[] trs = GameObject.Find("Canvas").transform.GetComponentsInChildren<Transform>(true);
                foreach (Transform tr in trs)
                {
                    if (tr.name == "IdleMenu")
                    {
                        tr.gameObject.SetActive(true);
                        break;
                    }
                }
                try
                {
                    using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
                    {
                        unityClass.CallStatic("showIdleMenu");
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message + "scale_in");
                }
            }
        }
		Qbi.transform.localScale = new Vector3 (Mathf.Lerp(Qbi_scale.x,Qbi_scale.x-30, time), Mathf.Lerp(Qbi_scale.y,Qbi_scale.y-30, time), Mathf.Lerp(Qbi_scale.z,Qbi_scale.z-30, time));
		Qbi.transform.localPosition = new Vector3 (Mathf.Lerp(Qbi_pos.x,Qbi_pos.x+Qbi_move_x, time), Mathf.Lerp(Qbi_pos.y,Qbi_pos.y-Qbi_move_y, time), Qbi.transform.localPosition.z);
		floor.transform.localScale = new Vector3 (Mathf.Lerp(floor_scale.x,floor_scale.x-0.25f, time), Mathf.Lerp(floor_scale.y,floor_scale.y-0.25f, time), transform.localScale.z);
		floor.transform.localPosition = new Vector3 (Mathf.Lerp(floor_pos.x,floor_pos.x+floor_move_x, time), Mathf.Lerp(floor_pos.y,floor_pos.y-floor_move_y, time), floor.transform.localPosition.z);
	}
	void scale_out ()
	{
		if (time <= 2)
		{
            time += 1f * Time.deltaTime;
		}
        else
        {
            if(done)
            {
                done = false;
                //好了 可以了 關閉
                StartCoroutine(callbackAgent("idleMenu", "close"));
            }
        }
        Qbi.transform.localScale = new Vector3(Mathf.Lerp(Qbi_scale.x, Qbi_scale.x + 30, time), Mathf.Lerp(Qbi_scale.y, Qbi_scale.y + 30, time), Mathf.Lerp(Qbi_scale.z, Qbi_scale.z + 30, time));
        Qbi.transform.localPosition = new Vector3(Mathf.Lerp(Qbi_pos.x, Qbi_pos.x - Qbi_move_x, time), Mathf.Lerp(Qbi_pos.y, Qbi_pos.y + Qbi_move_y, time), Qbi.transform.localPosition.z);
        floor.transform.localScale = new Vector3(Mathf.Lerp(floor_scale.x, floor_scale.x + 0.25f, time), Mathf.Lerp(floor_scale.y, floor_scale.y + 0.25f, time), transform.localScale.z);
        floor.transform.localPosition = new Vector3(Mathf.Lerp(floor_pos.x, floor_pos.x - floor_move_x, time), Mathf.Lerp(floor_pos.y, floor_pos.y + floor_move_y, time), floor.transform.localPosition.z);
        //Qbi.transform.localScale = new Vector3 (Mathf.Lerp(Qbi_scale.x-30,Qbi_scale.x, time), Mathf.Lerp(Qbi_scale.y-30,Qbi_scale.y, time), Mathf.Lerp(Qbi_scale.z-30,Qbi_scale.z, time));
        //Qbi.transform.localPosition = new Vector3 (Mathf.Lerp(Qbi_pos.x+Qbi_move_x,Qbi_pos.x, time), Mathf.Lerp(Qbi_pos.y-Qbi_move_y,Qbi_pos.y, time), Qbi.transform.localPosition.z);
        //floor.transform.localScale = new Vector3 (Mathf.Lerp(floor_scale.x-0.25f,floor_scale.x, time), Mathf.Lerp(floor_scale.y-0.25f,floor_scale.y, time), transform.localScale.z);
        //floor.transform.localPosition = new Vector3 (Mathf.Lerp(floor_pos.x+floor_move_x,floor_pos.x, time), Mathf.Lerp(floor_pos.y-floor_move_y,floor_pos.y, time), floor.transform.localPosition.z);
    }
	void time_reset ()
	{
        time = 0;
	}

    IEnumerator callbackAgent(string type, string message)
    {
        yield return new WaitForSeconds(1.0f);
        try
        {
            using (AndroidJavaClass unityClass = new AndroidJavaClass("com.mrmonsters.qbiengineagent.Agent"))
            {
                unityClass.CallStatic("callbackMessage", new string[] { type, message });
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message + "callbackAgent");
        }
    }
		
	void show_item (string xx , GameObject yy)
	{
		if (Qbi_ani.GetCurrentAnimatorStateInfo (0).IsName (xx)) 
		{
			yy.SetActive (true);
		} 
		else 
		{
			yy.SetActive (false);
		}
	}
}
