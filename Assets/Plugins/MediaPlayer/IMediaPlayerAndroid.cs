﻿using UnityEngine;
using System.Collections;

#if UNITY_ANDROID
public class IMediaPlayerAndroid : IMediaPlayer {

	AndroidJavaObject	mediaPlayer		= null;
	
	public void Init(string url, int width, int height,
        int left, int top)
	{
		mediaPlayer = new AndroidJavaObject("com.mrmonsters.qbiengineagent.VideoViewPlugin");
		SafeCall( "Init", url, width, height, left, top );
	}

	public void Term()
	{
		SafeCall( "Destroy" );
	}
	
	private void SafeCall( string method, params object[] args )
	{
		if( mediaPlayer != null )
		{
			mediaPlayer.Call( method, args );
		}
		//else
		//{
		//	Debug.LogError( "mediaPlayer is not created. you check is a call 'Init' method" );
		//}
	}
}
#endif