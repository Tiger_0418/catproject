﻿using UnityEngine;
using System.Collections;

public class MediaPlayerBehavior : MonoBehaviour {
	
	IMediaPlayer			videoView;
		
	#region Method
	
	public void Awake()
	{
		
		#if UNITY_ANDROID
		videoView = new IMediaPlayerAndroid();
		#elif UNITY_IPHONE
		videoView = new IMediaPlayerIOS();
		#else
		videoView = new IMediaPlayerNull();
		#endif
		
	}

	public void Init(string url, int width, int height, int left, int top) 
	{
		videoView.Init(url, width, height, left, top);
	}
	
	public void OnDestroy()
	{
		videoView.Term();
	}
	
	#endregion
}
