﻿using UnityEngine;
using System.Collections;

public interface IMediaPlayer
{
	
	void Init( string url, int width, int height, int left, int top);
	void Term();
	
}
