package com.mrmonsters.Qbi;

import com.mrmonsters.qbiengineagent.Agent;
import com.unity3d.player.*;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;

public class UnityPlayerActivity extends Activity
{
    private static String TAG = "UnityPlayerActivity";
    protected UnityPlayer mUnityPlayer = null; // don't change the name of this variable; referenced from native code
    private static boolean bClearApp = false;

    public void closeUnity() {
        Log.d(TAG, "unity closeUnity" );
        setResult(1);
        bClearApp = true;
        moveTaskToBack(true);
    }

    // Setup activity layout
    @Override protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        mUnityPlayer = new UnityPlayer(this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();

        //Agent.QbiSetting(this);
        Agent._callback.onCallbackUnityActivity(this);
        Log.d(TAG, "unity onCreate" );
    }

    @Override
    public void onBackPressed() {
        // your code.
        Log.d(TAG, "unity onBackPressed");
        setResult(1);
        bClearApp = true;
        moveTaskToBack(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("unity", "onActivityResult" + requestCode +"result"+ resultCode);

        if (requestCode == Agent.RC_START_SETTING) {
            if (resultCode == 1) {
                Log.d("unity", "getdata");
                Bundle b =  data.getExtras().getBundle("data");
                String command = String.format("%s|%s|%s|%s|%s",
                        b.getString("qbsize"),
                        b.getString("textspeed"),
                        b.getString("pagespeed"),
                        b.getString("btnsize"),
                        b.getString("oreitation")
                );
                UnityPlayer.UnitySendMessage("Main", "Initial", command);
            }
        } else {
            Agent._callback.onAcitivityResultCallback(requestCode, resultCode, data);
        }
    }

    @Override protected void onNewIntent(Intent intent)
    {
        // To support deep linking, we need to make sure that the client can get access to
        // the last sent intent. The clients access this through a JNI api that allows them
        // to get the intent set on launch. To update that after launch we have to manually
        // replace the intent with the one caught here.
        setIntent(intent);
    }

    // Quit Unity
    @Override protected void onDestroy ()
    {
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onDestroy");
            mUnityPlayer.quit();
        }
        super.onDestroy();
    }

    // Pause Unity
    @Override protected void onPause()
    {
        super.onPause();
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onPause");
            mUnityPlayer.pause();
        }
    }

    // Resume Unity
    @Override protected void onResume()
    {
        super.onResume();
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onResume");
            if(bClearApp) {
                Log.d(TAG, "unity onResume finish");
                finish();
            } else
                mUnityPlayer.resume();
        }
    }

    @Override protected void onStart()
    {
        super.onStart();
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onStart");
            mUnityPlayer.start();
        }
    }

    @Override protected void onStop()
    {
        super.onStop();
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onStop");
            mUnityPlayer.stop();
        }
    }

    // Low Memory Unity
    @Override public void onLowMemory()
    {
        super.onLowMemory();
        if(mUnityPlayer != null) {
            Log.d(TAG, "unity onLowMemory" );
            mUnityPlayer.lowMemory();
        }
    }

    // Trim Memory Unity
    @Override public void onTrimMemory(int level)
    {
        super.onTrimMemory(level);
        if (level == TRIM_MEMORY_RUNNING_CRITICAL)
        {
            Log.d(TAG, "unity onTrimMemory" );
            mUnityPlayer.lowMemory();
        }
    }

    // This ensures the layout will be correct.
    @Override public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override public boolean dispatchKeyEvent(KeyEvent event)
    {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     { return mUnityPlayer.injectEvent(event); }
    @Override public boolean onKeyDown(int keyCode, KeyEvent event)   {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // your code
            Log.d(TAG, "unity KEYCODE_BACK2");
            setResult(1);
            bClearApp = true;
            moveTaskToBack(true);
            return true;
        }
        return mUnityPlayer.injectEvent(event);
    }
    @Override public boolean onTouchEvent(MotionEvent event)          { return mUnityPlayer.injectEvent(event); }
    /*API12*/ public boolean onGenericMotionEvent(MotionEvent event)  { return mUnityPlayer.injectEvent(event); }
}
